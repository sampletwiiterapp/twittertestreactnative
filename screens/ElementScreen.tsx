import React, {Component} from 'react';
import {SafeAreaView, Text, StyleSheet, View, Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

type ElementScreenProps = {
  route: any;
};

type ElementScreenState = {
  tweet: {
    id: string;
    text: string;
  };
};

export default class ElementScreen extends Component<
  ElementScreenProps,
  ElementScreenState
> {
  constructor(props: ElementScreenProps) {
    super(props);
    this.state = {
      tweet: props.route.params.tweet,
    };
  }

  componentWillReceiveProps(nextProps: Readonly<ElementScreenProps>) {
    this.setState({
      tweet: nextProps.route.params.tweet,
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.tweet}>
          <Text style={styles.tweet_id}>ID: {this.state.tweet.id}</Text>
          <Text style={styles.tweet_text}>Text: {this.state.tweet.text}</Text>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tweet: {
    borderColor: 'rgb(196, 207, 214)',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 10,
    width: width - 30,
    marginLeft: 15,
    backgroundColor: '#ffffff',
    marginRight: 15,
    padding: 20,
    marginTop: 30,
  },
  tweet_id: {
    fontSize: 16,
  },
  tweet_text: {
    fontSize: 12,
  },
});
