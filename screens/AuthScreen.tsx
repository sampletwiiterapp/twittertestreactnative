import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';
import axios from 'axios';
import validTwitterUser from '../helpers/functions';
import {twitter_token} from '../helpers/variables';

const {width} = Dimensions.get('window');

type AuthScreenProps = {
  onSetUser: any;
};

type AuthScreenState = {
  nickname: string;
  loading: boolean;
};

export default class AuthScreen extends Component<
  AuthScreenProps,
  AuthScreenState
> {
  constructor(props: AuthScreenProps) {
    super(props);
    this.state = {
      nickname: '',
      loading: false,
    };
  }

  async submit() {
    let react = this;

    if (
      !validTwitterUser(react.state.nickname) ||
      react.state.nickname.trim().length < 4
    ) {
      Alert.alert('Invalid username', '');
      return;
    }

    react.setState({
      loading: true,
    });

    axios
      .get(
        'https://api.twitter.com/2/users/by/username/' + react.state.nickname,
        {
          headers: {
            Authorization: 'Bearer ' + twitter_token,
          },
        },
      )
      .then(function (response) {
        console.log(response);

        if (response.data.hasOwnProperty('errors')) {
          if (response.data.errors.length > 0) {
            react.setState(
              {
                loading: false,
              },
              () => {
                Alert.alert(
                  response.data.errors[0].title,
                  response.data.errors[0].detail,
                );
              },
            );
          }
        } else {
          react.props.onSetUser(response.data.data);
        }
      })
      .catch(function (error) {
        console.log(error.response);
        react.setState(
          {
            loading: false,
          },
          () => {
            Alert.alert('Network error');
          },
        );
      });
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <KeyboardAvoidingView
          style={styles.container}
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
          <Text style={styles.title}>Auth Screen</Text>
          {!this.state.loading ? (
            <>
              <TextInput
                style={styles.input}
                value={this.state.nickname}
                onChangeText={text => {
                  this.setState({
                    nickname: text,
                  });
                }}
              />
              <TouchableOpacity
                style={styles.button}
                onPress={this.submit.bind(this)}>
                <Text style={styles.button_text}>Sign In</Text>
              </TouchableOpacity>
            </>
          ) : (
            <ActivityIndicator style={styles.loader} color={'rgb(29, 161, 242)'} />
          )}
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 30,
  },
  input: {
    width: width - 60,
    borderStyle: 'solid',
    borderColor: '#cccccc',
    borderWidth: 1,
    marginTop: 40,
    borderRadius: 5,
    height: 30,
    marginLeft: 50,
    marginRight: 50,
    fontSize: 16,
    paddingLeft: 10,
    paddingRight: 10,
    padding: 0,
    color: '#000000',
  },
  button: {
    marginTop: 30,
    height: 40,
    marginLeft: 90,
    marginRight: 90,
    borderRadius: 5,
    width: width - 180,
    backgroundColor: 'rgb(29, 161, 242)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    color: '#ffffff',
    fontSize: 16,
  },
  loader: {
    marginTop: 30,
  },
});
