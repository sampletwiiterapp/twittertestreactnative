import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

const {width} = Dimensions.get('window');

type SettingsScreenProps = {
  user: any;
  onSetUser: any;
};

type SettingsScreenState = {
  user: any;
};

export default class UserScreen extends Component<
  SettingsScreenProps,
  SettingsScreenState
> {
  constructor(props: SettingsScreenProps) {
    super(props);
    this.state = {
      user: props.user,
    };
  }

  logout() {
    this.props.onSetUser(null);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.text}>ID: {this.state.user.id}</Text>
        <Text style={styles.text}>userame: {this.state.user.username}</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={this.logout.bind(this)}>
          <Text style={styles.button_text}>Sign Out</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginTop: 30,
    height: 40,
    marginLeft: 90,
    marginRight: 90,
    borderRadius: 5,
    width: width - 180,
    backgroundColor: 'rgb(29, 161, 242)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    color: '#ffffff',
    fontSize: 16,
  },
  text: {
    fontSize: 22,
    marginTop: 16,
  },
});
