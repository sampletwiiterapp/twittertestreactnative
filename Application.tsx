import React, {Component} from 'react';
import AuthScreen from './screens/AuthScreen';
import AuthStacks from './stacks/authStacks';

type ApplicationProps = {
  user: any;
  onSetUser: any;
};

type ApplicationState = {
  user: any;
};

export default class Application extends Component<
  ApplicationProps,
  ApplicationState
> {
  constructor(props: ApplicationProps) {
    super(props);
    this.state = {
      user: props.user,
    };
  }

  componentWillReceiveProps(nextProps: ApplicationProps) {
    this.setState({
      user: nextProps.user,
    });
  }

  render() {
    if (this.state.user === null) {
      return <AuthScreen onSetUser={this.props.onSetUser.bind(this)} />;
    }

    return (
      <AuthStacks
        user={this.state.user}
        onSetUser={this.props.onSetUser.bind(this)}
      />
    );
  }
}
