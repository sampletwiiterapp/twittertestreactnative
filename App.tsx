import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import Application from './Application';

type AppProps = {};
type AppState = {
  user: any;
};

class App extends Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props);
    this.state = {
      user: null,
    };
  }

  async componentDidMount() {
    let user = await AsyncStorage.getItem('user');

    if (user !== null) {
      this.setState({
        user,
      });
    }
  }

  setUser(user: any) {
    this.setState({
      user,
    });
  }

  render() {
    return (
      <NavigationContainer>
        <Application
          onSetUser={this.setUser.bind(this)}
          user={this.state.user}
        />
      </NavigationContainer>
    );
  }
}

export default App;
