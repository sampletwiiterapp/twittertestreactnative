import React, {Component, Fragment} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import UserScreen from '../screens/UserScreen';
import ElementsStacks from './elementsStacks';

const Tab = createBottomTabNavigator();

type AuthStacksProps = {
  user: any;
  onSetUser: any;
};

export default class AuthStacks extends Component<AuthStacksProps> {
  render() {
    return (
      <Fragment>
        <Tab.Navigator>
          <Tab.Screen
            name="Home"
            children={() => (
              <ElementsStacks
                user={this.props.user}
                onSetUser={this.props.onSetUser.bind(this)}
              />
            )}
          />
          <Tab.Screen
            name="Settings"
            children={() => (
              <UserScreen
                user={this.props.user}
                onSetUser={this.props.onSetUser.bind(this)}
              />
            )}
          />
        </Tab.Navigator>
      </Fragment>
    );
  }
}
