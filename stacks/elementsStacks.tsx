import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import ElementsScreen from '../screens/ElementsScreen';
import ElementScreen from '../screens/ElementScreen';

const Stack = createStackNavigator();

type ElementsScreenProps = {
  user: any;
  onSetUser: any;
};

export default class ElementsStacks extends Component<ElementsScreenProps> {
  render() {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="ElementsScreen"
          component={ElementsScreen}
          initialParams={{
            user: this.props.user,
          }}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen name="ElementScreen" component={ElementScreen} />
      </Stack.Navigator>
    );
  }
}
