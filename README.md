
Here's an instruction on how to run a test task

To run the application, go to the application directory and enter the following console commands

<i>yarn install</i>
<i>cd ios</i>
<i>pod install</i>
<i>cd ..</i>

To run ios <i>react-native run-ios</i>

To run android <i>react-native run-android</i>

https://reactnative.dev/docs/environment-setup -
Here is a detailed instruction on setting up the working environment.

