export default function validTwitterUser(sn: string) {
  return /^[a-zA-Z0-9_]{1,15}$/.test(sn);
}
